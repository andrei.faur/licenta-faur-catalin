from ultralytics import YOLO
import cv2
import easyocr
import string
import json

reader = easyocr.Reader(['en'], gpu=False)

dict_char_to_int = {'O': '0',
                    'I': '1',
                    'J': '3',
                    'A': '4',
                    'G': '6',
                    'S': '5'}

dict_int_to_char = {'0': 'O',
                    '1': 'I',
                    '3': 'J',
                    '4': 'A',
                    '6': 'G',
                    '5': 'S'}

county_abbreviation = {
        'AB', 'AR', 'AG', 'BC', 'BH', 'BN', 'BR', 'BT', 'BV', 'BZ',
        'CS', 'CL', 'CJ', 'CT', 'CV', 'DB', 'DJ', 'GL', 'GR', 'GJ',
        'HR', 'HD', 'IL', 'IS', 'IF', 'MM', 'MH', 'MS', 'NT', 'OT',
        'PH', 'SM', 'SJ', 'SB', 'SV', 'TR', 'TM', 'TL', 'VS', 'VL', 'VN'
}

def licenses_plate_format(text):

    is_license_plate_of_Bucharest = False
    is_license_plate_of_Bucharest_older = False
    is_license_plate_of_county = False
    license_plate_ = ''
    if  len(text) == 7 and \
        (text[0] in string.ascii_uppercase or text[0] in dict_int_to_char.keys()) and \
        (text[1] in string.ascii_uppercase or text[1] in dict_int_to_char.keys()) and \
        (text[2] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
        (text[3] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[3] in dict_char_to_int.keys()) and \
        (text[4] in string.ascii_uppercase or text[4] in dict_int_to_char.keys()) and \
        (text[5] in string.ascii_uppercase or text[5] in dict_int_to_char.keys()) and \
        (text[6] in string.ascii_uppercase or text[6] in dict_int_to_char.keys()):
        is_license_plate_of_county = True
    elif len(text) == 7 and \
         (text[0] == 'B') and \
         (text[1] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
         (text[2] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
         (text[3] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
         (text[4] in string.ascii_uppercase or text[4] in dict_int_to_char.keys()) and \
         (text[5] in string.ascii_uppercase or text[5] in dict_int_to_char.keys()) and \
         (text[6] in string.ascii_uppercase or text[6] in dict_int_to_char.keys()):
        is_license_plate_of_Bucharest = True
    elif len(text) == 6 and \
         (text[0] == 'B') and \
         (text[1] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
         (text[2] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'] or text[2] in dict_char_to_int.keys()) and \
         (text[3] in string.ascii_uppercase or text[4] in dict_int_to_char.keys()) and \
         (text[4] in string.ascii_uppercase or text[5] in dict_int_to_char.keys()) and \
         (text[5] in string.ascii_uppercase or text[6] in dict_int_to_char.keys()):
        is_license_plate_of_Bucharest_older = True
    else:
        return None

    if is_license_plate_of_county == True:
        license_plate_ = ''
        mapping = {0: dict_int_to_char, 1: dict_int_to_char, 4: dict_int_to_char, 5: dict_int_to_char,
                   6: dict_int_to_char, 2: dict_char_to_int, 3: dict_char_to_int}
        for j in [0, 1, 2, 3, 4, 5, 6]:
            if text[j] in mapping[j].keys():
                license_plate_ += mapping[j][text[j]]
            else:
                license_plate_ += text[j]
        if (license_plate_[:2] in county_abbreviation):
            return license_plate_
        else:
            return None
    elif is_license_plate_of_Bucharest == True:
        mapping = {0: dict_int_to_char, 4: dict_int_to_char, 5: dict_int_to_char,
                   6: dict_int_to_char,1: dict_char_to_int, 2: dict_char_to_int, 3: dict_char_to_int}
        for j in [0, 1, 2, 3, 4, 5, 6]:
            if text[j] in mapping[j].keys():
                license_plate_ += mapping[j][text[j]]
            else:
                license_plate_ += text[j]
        return license_plate_
    elif is_license_plate_of_Bucharest_older == True:
        mapping = {0: dict_int_to_char, 3: dict_int_to_char, 4: dict_int_to_char,
                   5: dict_int_to_char, 1: dict_char_to_int, 2: dict_char_to_int}
        for j in [0, 1, 2, 3, 4, 5]:
            if text[j] in mapping[j].keys():
                license_plate_ += mapping[j][text[j]]
            else:
                license_plate_ += text[j]
        return license_plate_
    else:
        return None



def read_license_plate(license_plate_crop):

    detections = reader.readtext(license_plate_crop)
    for detection in detections:
        bbox, text, score = detection

        text = text.upper().replace(' ', '')
        return licenses_plate_format(text)

    return None


license_plate_detector = YOLO('Modele_preantrenate/license_plate_detector.pt')

video_path = 'Dataset_oficial_pentru_licenta/Pentru_extragerea_de_numere_de_inmatriculare/Video_1_with_Iphone.MOV'
cap = cv2.VideoCapture(video_path)

license_plates_to_car = []

#read frames
frame_number = -1
ret = True
while ret:
    frame_number += 1
    ret, frame = cap.read()
    if ret:

        license_plates = license_plate_detector(frame)[0]


        for license_plate in license_plates.boxes.data.tolist():
            x1, y1, x2, y2, score, class_id = license_plate

            if score > 0.40:
                license_plate_crop = frame[int(y1):int(y2), int(x1): int(x2), :]


                license_plate_crop_gray = cv2.cvtColor(license_plate_crop, cv2.COLOR_BGR2GRAY)


                _, license_plate_crop_thresh = cv2.threshold(license_plate_crop_gray, 115, 255, cv2.THRESH_BINARY_INV)

                license_plate_text = read_license_plate(license_plate_crop_thresh)

                if license_plate_text is not None:
                    license_plates_to_car.append({
                        'license_plate' : license_plate_text,
                        'confidence': score
                    })
                json_license_plates = json.dumps(license_plates_to_car, indent=2)
                with open('Logs/LicensePlates.json', 'a') as file:
                    file.write(json_license_plates)

                x1 = int(x1)
                y1 = int(y1)
                x2 = int(x2)
                y2 = int(y2)

                cv2.rectangle(frame, (x1, y1), (x2, y2), (255, 0, 255), 2)
                label = f"ID: {license_plate_text}, Conf: {score:.2f}"
                cv2.putText(frame, label, (x1, y1 - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 255), 2)

        cv2.imshow('Video', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break