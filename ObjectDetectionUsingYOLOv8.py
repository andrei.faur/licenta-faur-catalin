import cv2
from mltu.torch.yolo.detectors.torch_detector import Detector as TorchDetector
import json
import numpy as np
from ultralytics.engine.model import Model as BaseModel

base_model = BaseModel("Modele_preantrenate/yolov8n.pt")

input_width, input_height = 640, 640
confidence_threshold = 0.5
iou_threshold = 0.5

detector = TorchDetector(base_model.model, input_width, input_height, base_model.names, confidence_threshold, iou_threshold)

video_path = 'Video_dataset_with_phone_camera/Video_2_change_size.mp4'

cap = cv2.VideoCapture(video_path)

frame_number = -1
ret = True
while ret:
    frame_number += 1
    ret, frame = cap.read()
    if ret:

        detections = detector(frame)

        frame = detections.applyToFrame(frame)

        detected_objects = []
        for detection in detections:
            class_name = detection.label
            confidence = detection.confidence
            box = detection.bbox

            detected_objects.append({
                'class_name': class_name,
                'confidence': np.float32(confidence),
                'bounding_box':  np.array(box, dtype=np.float32).tolist()
            })
            for obj in detected_objects:
                obj['confidence'] = float(obj['confidence'])
                obj['bounding_box'] = [float(coord) for coord in obj['bounding_box']]

            json_object_detection = json.dumps(detected_objects, indent=4)
            with open('Logs/ObjectDetection.json','a') as file:
                file.write(json_object_detection)

        cv2.imshow('Video', frame)
        if cv2.waitKey(2) & 0xFF == ord('q'):
            break